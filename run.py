import os
import sys

from sh import aws, pulumi  # noqa

aws('configure', 'set', 'aws_access_key_id', os.environ['STATE_BUCKET_ACCESS_KEY'])
aws('configure', 'set', 'aws_secret_access_key', os.environ['STATE_BUCKET_SECRET_KEY'])
aws('configure', 'set', 'region', os.environ['STATE_BUCKET_REGION'])

pulumi('login', f's3://{os.environ["STATE_BUCKET_ID"]}?endpoint={os.environ["STATE_BUCKET_ENDPOINT"]}')

pulumi(*sys.argv[1:], _out=sys.stdout, _err=sys.stderr)
