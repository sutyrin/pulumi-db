import os
import re

import pulumi
import pulumi_yandex_unofficial as yandex

from pulumi import automation as auto

config = pulumi.Config()

stack_name = pulumi.get_stack()
pulumi.log.debug(f'stack_name: {stack_name}')
env = re.match('meta\.(.*)', stack_name).groups()[0]
folder_name = f'env-{env}'
cloud_id = os.getenv('YC_CLOUD_ID')

folder = yandex.ResourcemanagerFolder(folder_name, cloud_id=cloud_id)
folder_id = folder.id

sa = yandex.IamServiceAccount("pulumi-state", folder_id=folder_id)

state_editor = yandex.ResourcemanagerFolderIamMember(
    "pulumi-state-editor",
    folder_id=folder_id,
    role="storage.editor",
    member=sa.id.apply(lambda _id: f"serviceAccount:{_id}"),
)

state_key = yandex \
    .IamServiceAccountStaticAccessKey(
    "pulumi-state-static-key",
    service_account_id=sa.id,
)

bucket_name = 'pulumi-state'
state_bucket = yandex.StorageBucket(
    bucket_name,
    access_key=state_key.access_key,
    secret_key=state_key.secret_key,
)

pulumi.export('env', env)
pulumi.export('YC_CLOUD_ID', cloud_id)
pulumi.export('YC_FOLDER_ID', folder_id)
pulumi.export('state_bucket_id', state_bucket.id)
pulumi.export('state_bucket_access_key', state_key.access_key)
pulumi.export('state_bucket_secret_key', state_key.secret_key)

