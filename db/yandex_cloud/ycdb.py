import logging
from collections import namedtuple
from typing import Any, Optional

import pulumi
from pulumi import ResourceOptions, log
from pulumi.dynamic import CreateResult, ReadResult, Resource, ResourceProvider

from yc_cli import call_yc

log = logging.getLogger(__name__)
logging.basicConfig(filename='/tmp/pulumi.log', level=logging.DEBUG)


@pulumi.input_type
class MdbPostgresqlClusterInputs:
    def __init__(
        self, folder_id, db_name, user_name, user_password, zone, subnet_id, network_id,
        cluster_name, assign_public_ip=False,
    ):
        self.folder_id = folder_id
        self.db_name = db_name
        self.user_name = user_name
        self.user_password = user_password
        self.zone = zone
        self.subnet_id = subnet_id
        self.network_id = network_id
        self.cluster_name = cluster_name
        self.postgresql_version = '14'
        self.cluster_desc = ''
        self.assign_public_ip = assign_public_ip


class MdbPostgresqlProvider(ResourceProvider):
    def create(self, inputs: MdbPostgresqlClusterInputs) -> CreateResult:
        log.debug('started')
        log.debug(str(inputs))
        if type(inputs) == dict:
            inputs = {k: inputs[k] for k in inputs.keys() if not k.startswith('__')}
            inputs = namedtuple('GenericDict', inputs.keys())(**inputs)
        log.debug(str(inputs))

        environment = 'production'
        db_locale = 'en_US.UTF-8'
        pg_version = 14
        disk_type = 'network-hdd'
        disk_gb = 10
        extensions = 'hstore='

        creation_result = call_yc(
            'postgres', 'cluster', 'create',
            name=inputs.cluster_name,
            environment=environment,
            network_id=inputs.network_id,
            host=f'zone-id={inputs.zone},subnet-id={inputs.subnet_id},assign-public-ip={inputs.assign_public_ip and 1 or 0}',
            user=f'name={inputs.user_name},password={inputs.user_password}',
            database=f'name={inputs.db_name},owner={inputs.user_name},lc-collate={db_locale},lc-ctype={db_locale}',
            datatransfer_access=True,
            postgresql_version=pg_version,
            disk_type=disk_type,
            disk_size=f'{disk_gb}G'
        )

        configure_result = call_yc(
            'postgres', 'database', 'update',
            inputs.db_name,
            extensions=extensions,
            cluster_id=creation_result['id']
        )

        return CreateResult(id_=creation_result['id'], outs=creation_result)

    def read(self, id_: str, inputs: Any) -> ReadResult:
        result = call_yc('postgres', 'cluster', 'get', id_)
        return ReadResult(id_=result and id_, outs=result or {})

    def delete(self, id_: str, _props: Any) -> None:
        call_yc('postgres', 'cluster', 'delete', id_)


class MdbPostgresqlCluster(Resource):
    def __init__(self, name, props: MdbPostgresqlClusterInputs,
                 opts: Optional[ResourceOptions] = None):
        log.debug(f'ctor {str(vars(props))}')
        super().__init__(MdbPostgresqlProvider(), name, {**vars(props)}, opts)
