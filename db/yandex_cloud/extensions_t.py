import os

import backoff as backoff
import psycopg2
import pytest
from pulumi import automation as auto
from pulumi import log

pytestmark = pytest.mark.yc


@pytest.fixture(scope='module')
def stack():
    work_dir = os.path.join(os.path.dirname(__file__))
    try:
        stack = auto.select_stack(stack_name="ci.db", work_dir=work_dir)
        stack.cancel()
    except auto.errors.StackNotFoundError:
        stack = auto.create_stack(stack_name="ci.db", work_dir=work_dir)
    return stack


@backoff.on_exception(backoff.constant, psycopg2.OperationalError, interval=0.3, max_time=60 * 15)
def wait_for_ready(uri):
    psycopg2.connect(uri, connect_timeout=1)


@pytest.fixture(scope='module')
def db(stack):
    stack.destroy(on_output=print)
    stack.up(on_output=print)
    uri = stack.outputs()['uri'].value
    wait_for_ready(uri)

    yield uri

    stack.destroy(on_output=print)


@pytest.fixture(scope='module')
def extensions(db):
    sql = "select extname from pg_extension"

    conn = psycopg2.connect(db)

    with conn.cursor() as cursor:
        cursor.execute(sql)
        rows = cursor.fetchall()
        extensions = [r[0] for r in rows]
        log.info(f'got extensions: {extensions}')

        return extensions


def hstore_is_present_t(extensions):
    assert 'hstore' in extensions
