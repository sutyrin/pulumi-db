import sys

import psycopg2

tables = """
common_requesthistory
common_producthistory
common_tradeinhistory
common_multipliersoverridehistory
common_sellpointmultipliersoverridehistory
common_multipliershistory
common_otmultipliersoverridehistory
common_variantshistory
common_producthistorydeleted
common_crmproducthistory
common_crmmultipliershistoryproviders
common_tradeinhistorydeleted
common_requesthistorydeleted
common_buybackpricemultipliershistory
common_partnerhistory
common_scripthistoryentry
common_crmproductversions
common_tradeinversions
common_spcart
django_session
common_cities
common_crmrequestmarketingall
common_imeibuyoutprices
common_cdekorderitems
common_cdekorderservices
common_transactionitems
common_sellingreconciliationproducts
common_sipcalllog

""".strip().splitlines()


def truncate_tables(db_dsn, tables=tables):
    with psycopg2.connect(db_dsn) as conn:
        c = conn.cursor()
        for table in tables:
            print(f'truncating {table}...')
            c.execute(f'truncate table {table}')
        conn.commit()


if __name__ == '__main__':
    truncate_tables(sys.argv[1])
