```
INFO:root:Done Yandex.Cloud operation. ID: c9qvtl4q5hrc9qam0qlu. Response: id: "c9qiotsbf5qjpq0j8o18"
folder_id: "b1gf9hp1ej1sqbo3f6rf"
created_at {
  seconds: 1665321511
  nanos: 613614000
}
name: "ringio"
environment: PRODUCTION
monitoring {
  name: "Console"
  description: "Console charts"
  link: "https://console.cloud.yandex.ru/folders/b1gf9hp1ej1sqbo3f6rf/managed-postgresql/cluster/c9qiotsbf5qjpq0j8o18?section=monitoring"
}
config {
  version: "14"
  resources {
    resource_preset_id: "s2.micro"
    disk_size: 10737418240
    disk_type_id: "network-ssd"
  }
  autofailover {
    value: true
  }
  backup_window_start {
    hours: 22
  }
  access {
  }
  performance_diagnostics {
    sessions_sampling_interval: 60
    statements_sampling_interval: 600
  }
  postgresql_config_14 {
    effective_config {
      max_connections {
        value: 400
      }
      shared_buffers {
        value: 2147483648
      }
      temp_buffers {
        value: 8388608
      }
      max_prepared_transactions {
      }
      work_mem {
        value: 4194304
      }
      maintenance_work_mem {
        value: 67108864
      }
      autovacuum_work_mem {
        value: -1
      }
      temp_file_limit {
        value: -1
      }
      vacuum_cost_delay {
      }
      vacuum_cost_page_hit {
        value: 1
      }
      vacuum_cost_page_miss {
        value: 10
      }
      vacuum_cost_page_dirty {
        value: 20
      }
      vacuum_cost_limit {
        value: 200
      }
      bgwriter_delay {
        value: 200
      }
      bgwriter_lru_maxpages {
        value: 100
      }
      bgwriter_lru_multiplier {
        value: 2.0
      }
      bgwriter_flush_after {
        value: 524288
      }
      backend_flush_after {
      }
      old_snapshot_threshold {
        value: -1
      }
      wal_level: WAL_LEVEL_LOGICAL
      synchronous_commit: SYNCHRONOUS_COMMIT_ON
      checkpoint_timeout {
        value: 300000
      }
      checkpoint_completion_target {
        value: 0.5
      }
      checkpoint_flush_after {
        value: 262144
      }
      max_wal_size {
        value: 1073741824
      }
      min_wal_size {
        value: 536870912
      }
      max_standby_streaming_delay {
        value: 30000
      }
      default_statistics_target {
        value: 1000
      }
      constraint_exclusion: CONSTRAINT_EXCLUSION_PARTITION
      cursor_tuple_fraction {
        value: 0.1
      }
      from_collapse_limit {
        value: 8
      }
      join_collapse_limit {
        value: 8
      }
      force_parallel_mode: FORCE_PARALLEL_MODE_OFF
      client_min_messages: LOG_LEVEL_NOTICE
      log_min_messages: LOG_LEVEL_WARNING
      log_min_error_statement: LOG_LEVEL_ERROR
      log_min_duration_statement {
        value: -1
      }
      log_checkpoints {
      }
      log_connections {
      }
      log_disconnections {
      }
      log_duration {
      }
      log_error_verbosity: LOG_ERROR_VERBOSITY_DEFAULT
      log_lock_waits {
      }
      log_statement: LOG_STATEMENT_NONE
      log_temp_files {
        value: -1
      }
      search_path: "\"$user\", public"
      row_security {
        value: true
      }
      default_transaction_isolation: TRANSACTION_ISOLATION_READ_COMMITTED
      statement_timeout {
      }
      lock_timeout {
      }
      idle_in_transaction_session_timeout {
      }
      bytea_output: BYTEA_OUTPUT_HEX
      xmlbinary: XML_BINARY_BASE64
      xmloption: XML_OPTION_CONTENT
      gin_pending_list_limit {
        value: 4194304
      }
      deadlock_timeout {
        value: 1000
      }
      max_locks_per_transaction {
        value: 64
      }
      max_pred_locks_per_transaction {
        value: 64
      }
      array_nulls {
        value: true
      }
      backslash_quote: BACKSLASH_QUOTE_SAFE_ENCODING
      default_with_oids {
      }
      escape_string_warning {
        value: true
      }
      lo_compat_privileges {
      }
      quote_all_identifiers {
      }
      standard_conforming_strings {
        value: true
      }
      synchronize_seqscans {
        value: true
      }
      transform_null_equals {
      }
      exit_on_error {
      }
      seq_page_cost {
        value: 1.0
      }
      random_page_cost {
        value: 1.0
      }
      autovacuum_max_workers {
        value: 3
      }
      autovacuum_vacuum_cost_delay {
        value: 45
      }
      autovacuum_vacuum_cost_limit {
        value: 700
      }
      autovacuum_naptime {
        value: 15000
      }
      archive_timeout {
        value: 30000
      }
      track_activity_query_size {
        value: 1024
      }
      enable_bitmapscan {
        value: true
      }
      enable_hashagg {
        value: true
      }
      enable_hashjoin {
        value: true
      }
      enable_indexscan {
        value: true
      }
      enable_indexonlyscan {
        value: true
      }
      enable_material {
        value: true
      }
      enable_mergejoin {
        value: true
      }
      enable_nestloop {
        value: true
      }
      enable_seqscan {
        value: true
      }
      enable_sort {
        value: true
      }
      enable_tidscan {
        value: true
      }
      max_worker_processes {
        value: 8
      }
      max_parallel_workers {
        value: 8
      }
      max_parallel_workers_per_gather {
        value: 2
      }
      autovacuum_vacuum_scale_factor {
        value: 1e-05
      }
      autovacuum_analyze_scale_factor {
        value: 0.0001
      }
      default_transaction_read_only {
      }
      timezone: "Europe/Moscow"
      enable_parallel_append {
        value: true
      }
      enable_parallel_hash {
        value: true
      }
      enable_partition_pruning {
        value: true
      }
      enable_partitionwise_aggregate {
      }
      enable_partitionwise_join {
      }
      max_parallel_maintenance_workers {
        value: 2
      }
      parallel_leader_participation {
        value: true
      }
      effective_io_concurrency {
        value: 1
      }
      effective_cache_size {
        value: 107374182400
      }
      auto_explain_log_min_duration {
        value: -1
      }
      auto_explain_log_analyze {
      }
      auto_explain_log_buffers {
      }
      auto_explain_log_timing {
      }
      auto_explain_log_triggers {
      }
      auto_explain_log_verbose {
      }
      auto_explain_log_nested_statements {
      }
      auto_explain_sample_rate {
        value: 1.0
      }
      pg_hint_plan_enable_hint {
        value: true
      }
      pg_hint_plan_enable_hint_table {
      }
      pg_hint_plan_debug_print: PG_HINT_PLAN_DEBUG_PRINT_OFF
      max_slot_wal_keep_size {
        value: -1
      }
    }
    user_config {
    }
    default_config {
      max_connections {
        value: 400
      }
      shared_buffers {
        value: 2147483648
      }
      temp_buffers {
        value: 8388608
      }
      max_prepared_transactions {
      }
      work_mem {
        value: 4194304
      }
      maintenance_work_mem {
        value: 67108864
      }
      autovacuum_work_mem {
        value: -1
      }
      temp_file_limit {
        value: -1
      }
      vacuum_cost_delay {
      }
      vacuum_cost_page_hit {
        value: 1
      }
      vacuum_cost_page_miss {
        value: 10
      }
      vacuum_cost_page_dirty {
        value: 20
      }
      vacuum_cost_limit {
        value: 200
      }
      bgwriter_delay {
        value: 200
      }
      bgwriter_lru_maxpages {
        value: 100
      }
      bgwriter_lru_multiplier {
        value: 2.0
      }
      bgwriter_flush_after {
        value: 524288
      }
      backend_flush_after {
      }
      old_snapshot_threshold {
        value: -1
      }
      wal_level: WAL_LEVEL_LOGICAL
      synchronous_commit: SYNCHRONOUS_COMMIT_ON
      checkpoint_timeout {
        value: 300000
      }
      checkpoint_completion_target {
        value: 0.5
      }
      checkpoint_flush_after {
        value: 262144
      }
      max_wal_size {
        value: 1073741824
      }
      min_wal_size {
        value: 536870912
      }
      max_standby_streaming_delay {
        value: 30000
      }
      default_statistics_target {
        value: 1000
      }
      constraint_exclusion: CONSTRAINT_EXCLUSION_PARTITION
      cursor_tuple_fraction {
        value: 0.1
      }
      from_collapse_limit {
        value: 8
      }
      join_collapse_limit {
        value: 8
      }
      force_parallel_mode: FORCE_PARALLEL_MODE_OFF
      client_min_messages: LOG_LEVEL_NOTICE
      log_min_messages: LOG_LEVEL_WARNING
      log_min_error_statement: LOG_LEVEL_ERROR
      log_min_duration_statement {
        value: -1
      }
      log_checkpoints {
      }
      log_connections {
      }
      log_disconnections {
      }
      log_duration {
      }
      log_error_verbosity: LOG_ERROR_VERBOSITY_DEFAULT
      log_lock_waits {
      }
      log_statement: LOG_STATEMENT_NONE
      log_temp_files {
        value: -1
      }
      search_path: "\"$user\", public"
      row_security {
        value: true
      }
      default_transaction_isolation: TRANSACTION_ISOLATION_READ_COMMITTED
      statement_timeout {
      }
      lock_timeout {
      }
      idle_in_transaction_session_timeout {
      }
      bytea_output: BYTEA_OUTPUT_HEX
      xmlbinary: XML_BINARY_BASE64
      xmloption: XML_OPTION_CONTENT
      gin_pending_list_limit {
        value: 4194304
      }
      deadlock_timeout {
        value: 1000
      }
      max_locks_per_transaction {
        value: 64
      }
      max_pred_locks_per_transaction {
        value: 64
      }
      array_nulls {
        value: true
      }
      backslash_quote: BACKSLASH_QUOTE_SAFE_ENCODING
      default_with_oids {
      }
      escape_string_warning {
        value: true
      }
      lo_compat_privileges {
      }
      quote_all_identifiers {
      }
      standard_conforming_strings {
        value: true
      }
      synchronize_seqscans {
        value: true
      }
      transform_null_equals {
      }
      exit_on_error {
      }
      seq_page_cost {
        value: 1.0
      }
      random_page_cost {
        value: 1.0
      }
      autovacuum_max_workers {
        value: 3
      }
      autovacuum_vacuum_cost_delay {
        value: 45
      }
      autovacuum_vacuum_cost_limit {
        value: 700
      }
      autovacuum_naptime {
        value: 15000
      }
      archive_timeout {
        value: 30000
      }
      track_activity_query_size {
        value: 1024
      }
      enable_bitmapscan {
        value: true
      }
      enable_hashagg {
        value: true
      }
      enable_hashjoin {
        value: true
      }
      enable_indexscan {
        value: true
      }
      enable_indexonlyscan {
        value: true
      }
      enable_material {
        value: true
      }
      enable_mergejoin {
        value: true
      }
      enable_nestloop {
        value: true
      }
      enable_seqscan {
        value: true
      }
      enable_sort {
        value: true
      }
      enable_tidscan {
        value: true
      }
      max_worker_processes {
        value: 8
      }
      max_parallel_workers {
        value: 8
      }
      max_parallel_workers_per_gather {
        value: 2
      }
      autovacuum_vacuum_scale_factor {
        value: 1e-05
      }
      autovacuum_analyze_scale_factor {
        value: 0.0001
      }
      default_transaction_read_only {
      }
      timezone: "Europe/Moscow"
      enable_parallel_append {
        value: true
      }
      enable_parallel_hash {
        value: true
      }
      enable_partition_pruning {
        value: true
      }
      enable_partitionwise_aggregate {
      }
      enable_partitionwise_join {
      }
      max_parallel_maintenance_workers {
        value: 2
      }
      parallel_leader_participation {
        value: true
      }
      effective_io_concurrency {
        value: 1
      }
      effective_cache_size {
        value: 107374182400
      }
      auto_explain_log_min_duration {
        value: -1
      }
      auto_explain_log_analyze {
      }
      auto_explain_log_buffers {
      }
      auto_explain_log_timing {
      }
      auto_explain_log_triggers {
      }
      auto_explain_log_verbose {
      }
      auto_explain_log_nested_statements {
      }
      auto_explain_sample_rate {
        value: 1.0
      }
      pg_hint_plan_enable_hint {
        value: true
      }
      pg_hint_plan_enable_hint_table {
      }
      pg_hint_plan_debug_print: PG_HINT_PLAN_DEBUG_PRINT_OFF
      max_slot_wal_keep_size {
        value: -1
      }
    }
  }
  backup_retain_period_days {
    value: 7
  }
}
network_id: "enp4si1k10acn1unicd9"
maintenance_window {
  anytime {
  }
}

```
