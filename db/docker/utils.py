import re

import backoff as backoff
import psycopg2


@backoff.on_exception(backoff.constant, psycopg2.OperationalError, interval=0.3, max_time=10)
def wait_for_ready(uri):
    psycopg2.connect(uri, connect_timeout=1)


def url_parts(url):
    return re.match(
        (
            'postgresql://'
            '(?P<username>[^:]*):(?P<password>[^@]*)'
            '@(?P<host>[^:]*):(?P<port>[^/]*).*'
            '/(?P<dbname>.*)'
        ),
        url,
    ).groupdict()
