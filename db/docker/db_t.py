import os
import sys
from io import BytesIO

import psycopg2
import pytest
from sh import pg_dump, pg_restore, psql  # noqa

from .utils import url_parts, wait_for_ready

pytestmark = pytest.mark.docker

def may_be_queried_t(db):
    with psycopg2.connect(db).cursor() as c:
        c.execute('select 1')
        r = c.fetchone()[0]
        assert r == 1


def has_data_available_t(db, data):
    with psycopg2.connect(db).cursor() as c:
        c.execute('select id from t order by id')
        assert c.fetchall() == [(1,), (2,), (3,)]


def may_be_dumped_to_sql_t(db, data):
    dump = BytesIO()
    password = url_parts(db)['password']
    pg_dump('-d', db, _out=dump, _err=sys.stderr, _env={'PGPASSWORD': password})

    assert b'COPY public.t (id) FROM stdin' in dump.getvalue()


def may_be_dumped_and_restored_from_sql_t(stack, db, data):
    dump = BytesIO()
    password = url_parts(db)['password']
    pg_dump('-d', db, '-c', _out=dump, _err=sys.stderr, _env={'PGPASSWORD': password})

    assert b'COPY public.t (id) FROM stdin' in dump.getvalue()

    dump.seek(0)

    stack.cancel()
    stack.destroy(on_output=print)
    stack.up(on_output=print)
    db = stack.outputs()['uri'].value
    wait_for_ready(db)
    password = url_parts(db)['password']

    psql(db, _in=dump, _err=sys.stderr, _env={'PGPASSWORD': password})

    with psycopg2.connect(db).cursor() as c:
        c.execute('select id from t order by id')
        assert c.fetchall() == [(1,), (2,), (3,)]


def may_be_dumped_to_dir_t(db, data, dump_dir):
    pg_dump('-d', db, '-c', '-Fd', '-f', dump_dir, _out=sys.stdout, _err=sys.stderr)

    assert os.path.exists(dump_dir / 'toc.dat')


def may_be_dumped_and_restored_from_dir_t(stack, db, data, dump_dir):
    pg_dump('-d', db, '-c', '-Fd', '-f', dump_dir, _out=sys.stdout, _err=sys.stderr)

    stack.cancel()
    stack.destroy(on_output=print)
    stack.up(on_output=print)
    db = stack.outputs()['uri'].value
    wait_for_ready(db)

    password = url_parts(db)['password']
    out = BytesIO()
    pg_restore(
        '-d', db, '-v', '--no-owner', dump_dir,
        _out=out, _err=sys.stderr, _env={'PGPASSWORD': password},
    )

    with psycopg2.connect(db).cursor() as c:
        c.execute('select id from t order by id')
        assert c.fetchall() == [(1,), (2,), (3,)]


