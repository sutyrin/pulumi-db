import sys
from io import BytesIO

import pytest
from sh import pg_dump, s3cmd, tar  # noqa

pytestmark = pytest.mark.integration


def may_be_dumped_to_s3_t(db, data, s3_bucket, s3_stack, s3_bucket_id, s3_cfg_path, dump_dir):
    pg_dump('-d', db, '-Z6', '-c', '-Fd', '-f', dump_dir, _out=sys.stdout, _err=sys.stderr)

    dump_filename = 'dump.tar'

    s3cmd(
        tar('-c', dump_dir, _piped=True),
        '--access_key', s3_stack.outputs()['access_key'].value,
        '--secret_key', s3_stack.outputs()['secret_key'].value,
        '-c', s3_cfg_path,
        'put', '-', f's3://{s3_bucket_id}/{dump_filename}',
    )

    buffer = BytesIO()
    s3_bucket.Object(dump_filename).download_fileobj(buffer)
    read_content = buffer.getvalue()

    assert len(read_content) > 0
