import os

import boto3 as boto3
import psycopg2
import pytest
from pulumi import automation as auto

from .utils import wait_for_ready


@pytest.fixture
def dump_dir(tmpdir):
    return tmpdir / 'dbdump'


@pytest.fixture(scope='module')
def stack():
    work_dir = os.path.join(os.path.dirname(__file__))
    try:
        stack = auto.select_stack(stack_name="dev.db", work_dir=work_dir)
        stack.cancel()
    except auto.errors.StackNotFoundError:
        stack = auto.create_stack(stack_name="dev.db", work_dir=work_dir)
    return stack


@pytest.fixture(scope='module')
def db(stack):
    stack.destroy(on_output=print)  # needed only in unclean dev environment
    stack.up(on_output=print)

    uri = stack.outputs()['uri'].value
    wait_for_ready(uri)

    yield uri

    stack.destroy(on_output=print)


@pytest.fixture(scope='module')
def data(db):
    with psycopg2.connect(db).cursor() as c:
        c.execute('create table t (id integer)')
        c.execute('insert into t values (1), (2), (3)')
        c.execute('commit')


@pytest.fixture(scope='module')
def s3_stack():
    work_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'db_backup')
    try:
        stack = auto.select_stack(stack_name="dev.db_backup", work_dir=work_dir)
        stack.cancel()
    except auto.errors.StackNotFoundError:
        stack = auto.create_stack(stack_name="dev.db_backup", work_dir=work_dir)

    stack.destroy(on_output=print)  # needed only in unclean dev environment
    stack.up(on_output=print)
    stack.refresh()

    yield stack

    stack.destroy(on_output=print)


@pytest.fixture(scope='module')
def s3(s3_stack):
    return boto3.Session(
        aws_access_key_id=s3_stack.outputs()['access_key'].value,
        aws_secret_access_key=s3_stack.outputs()['secret_key'].value,
    ).resource(
        's3',
        endpoint_url='https://storage.yandexcloud.net',
    )


@pytest.fixture(scope='module')
def s3_bucket(s3, s3_stack):
    bucket_name = s3_stack.outputs()['bucket_id'].value
    return s3.Bucket(bucket_name)


@pytest.fixture(scope='module')
def s3_bucket_id(s3, s3_stack):
    return s3_stack.outputs()['bucket_id'].value


s3_cfg_content = """[default]
host_base = https://storage.yandexcloud.net
host_bucket =
"""


@pytest.fixture()
def s3_cfg_path(s3_stack, tmpdir):
    file_path = tmpdir / 's3cfg'

    with open(file_path, 'wt') as f:
        f.write(s3_cfg_content)

    return file_path
