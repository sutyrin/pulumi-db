from io import BytesIO

import pytest
from sh import echo, s3cmd  # noqa

pytestmark = pytest.mark.yc


def may_upload_by_sdk_t(s3_bucket):
    content = b'12131231231'
    filename = 'filename'
    s3_bucket.upload_fileobj(BytesIO(content), filename)
    buffer = BytesIO()
    s3_bucket.Object(filename).download_fileobj(buffer)
    read_content = buffer.getvalue()

    assert content == read_content


def may_upload_by_s3cmd_t(s3_bucket_id, s3_stack, s3_cfg_path, s3_bucket):
    filename = 'test.txt'
    content = b'asdfavasdfs'

    s3cmd(
        echo('-n', content),
        '--access_key', s3_stack.outputs()['access_key'].value,
        '--secret_key', s3_stack.outputs()['secret_key'].value,
        '-c', s3_cfg_path,
        'put', '-', f's3://{s3_bucket_id}/{filename}',
    )
    buffer = BytesIO()
    s3_bucket.Object(filename).download_fileobj(buffer)
    read_content = buffer.getvalue()

    assert read_content == content
