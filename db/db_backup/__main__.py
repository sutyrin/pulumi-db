import os

import pulumi
import pulumi_yandex_unofficial as yandex

bucket_name = 'test-bucket'
folder_id = os.getenv('YC_FOLDER_ID')

sa = yandex.IamServiceAccount("test-sa")
sa_editor = yandex.ResourcemanagerFolderIamMember(
    "sa-editor",
    folder_id=folder_id,
    role="storage.editor",
    member=sa.id.apply(lambda _id: f"serviceAccount:{_id}"),
)
sa_static_key = yandex \
    .IamServiceAccountStaticAccessKey(
    "sa-static-key",
    service_account_id=sa.id,
    description="static access key for object storage",
)

bucket = yandex.StorageBucket(
    bucket_name,
    force_destroy=True,
    access_key=sa_static_key.access_key,
    secret_key=sa_static_key.secret_key,
)

bucket_id = bucket.id

pulumi.export('bucket_name', bucket_name)
pulumi.export('bucket_id', bucket_id)
pulumi.export('access_key', sa_static_key.access_key)
pulumi.export('secret_key', sa_static_key.secret_key)
