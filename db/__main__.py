import pulumi

env = pulumi.get_stack().split('.')[0]

if env == 'dev':
    import pulumi_docker as docker

    user = 'postgres'
    password = '12345'
    port = 5432
    dbname = 'postgres'

    image = docker.RemoteImage('spacediver', name='spacediver/postgres-9.6-novolume')
    container = docker.Container(
        'db',
        image=image.latest,
        envs=[f'POSTGRES_PASSWORD={password}'],
        rm=False,
    )

    host = container.network_datas[0].ip_address
    uri = pulumi.Output.concat(
        'postgresql://', user, ':', password, '@', host, ':', str(port), '/', dbname)

    pulumi.export('uri', uri)

elif env in ['ci', 'prestable', 'production']:
    import os

    import pulumi_yandex_unofficial as yandex

    folder_id = os.environ.get('YC_FOLDER_ID')
    if not folder_id:
        raise Exception('please set YC_FOLDER_ID')

    zone = 'ru-central1-a'
    assign_public_ip = True
    host_preset = "s2.micro"
    lc_collate = "en_US.UTF-8"
    lc_type = lc_collate

    cluster_name = f'{env}-db'
    user_name = 'user'
    user_password = 'test_password'

    environment = 'PRODUCTION'
    db_locale = 'en_US.UTF-8'
    pg_version = '14'
    disk_type = 'network-hdd'
    disk_gb = 10
    extensions = ['hstore']

    foo_vpc_network = yandex.VpcNetwork("fooVpcNetwork")
    foo_vpc_subnet = yandex.VpcSubnet("fooVpcSubnet",
                                      zone="ru-central1-a",
                                      network_id=foo_vpc_network.id,
                                      v4_cidr_blocks=["10.5.0.0/24"])

    cluster = yandex.MdbPostgresqlCluster(
        "fooMdbPostgresqlCluster",
        environment=environment,
        network_id=foo_vpc_network.id,
        config=yandex.MdbPostgresqlClusterConfigArgs(
            version=pg_version,
            resources=yandex.MdbPostgresqlClusterConfigResourcesArgs(
                resource_preset_id=host_preset,
                disk_type_id=disk_type,
                disk_size=disk_gb,
            ),
        ),
        hosts=[
            yandex.MdbPostgresqlClusterHostArgs(
                zone=zone,
                subnet_id=foo_vpc_subnet.id,
                assign_public_ip=assign_public_ip,
            )],
    )

    db_user = yandex.MdbPostgresqlUser(
        "foomdbPostgresqlUser",
        cluster_id=cluster.id,
        password=user_password,
    )

    database = yandex.MdbPostgresqlDatabase(
        "foomdbPostgresqlDatabase",
        cluster_id=cluster.id,
        owner=db_user.name,
        lc_collate=lc_collate,
        lc_type=lc_type,
        extensions=[
            yandex.MdbPostgresqlDatabaseExtensionArgs(
                name=extension,
            ) for extension in extensions
        ],
    )

    port = 6432

    uri = pulumi.Output.concat(
        'postgresql://', db_user.name, ':', user_password,
        '@', cluster.hosts[0].fqdn, ':', str(port),
        '/', database.name,
    )

    pulumi.export('health', cluster.health)
    pulumi.export('uri', uri)

else:
    raise NotImplementedError(f'Unimplemented env: {env}')
