FROM ubuntu:jammy

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update

RUN apt-get install -y \
    jq \
    awscli \
    openssh-client \
    python3 \
    python3-venv \
    python3-pip \
    postgresql-common \
    postgresql-client-14 \
    libpq-dev \
    curl

RUN curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
RUN mv /root/yandex-cloud/bin/yc /usr/bin

RUN curl -fsSL https://get.pulumi.com | sh
RUN mv /root/.pulumi/bin/* /usr/bin

WORKDIR /app

ADD ./requirements.txt .

RUN pip install -r requirements.txt

ADD . .

#ENTRYPOINT python3 /app/run.py
