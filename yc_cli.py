import json
import os
from io import BytesIO

import sh


def call_yc(*args, **kwargs):
    folder_id = os.environ.get('YC_FOLDER_ID')
    token = os.getenv('YC_TOKEN')
    stderr = BytesIO()
    try:
        output = str(
            sh.yc(
                *args,
                folder_id=folder_id,
                token=token,
                format='json',
                _err=stderr,
                **kwargs
            )
        )
    except sh.ErrorReturnCode_1:  # noqa
        if b'not found' in stderr.getvalue():
            return
        else:
            raise
    if output:
        return json.loads(output)
