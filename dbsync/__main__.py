import os

import pulumi
import pulumi_yandex_unofficial as yandex
from pulumi import Output

from yandex_cloud.ycbinding import InstanceAddressBinding, InstanceAddressBindingInputs

cwd = os.path.abspath(os.path.dirname(__name__))
zone = 'ru-central1-a'

config = pulumi.Config()

proxy_host = config.get('proxy_host')
proxy_ssh_user = config.get('proxy_ssh_user')
db_config = config.get_object('db')

# db_uri = pulumi.Output.concat(
#     'postgresql://', db_config['user'], ':', db_config['password'],
#     '@', db_config['host'], ':', str(db_config['port']),
# )

if not proxy_host or not proxy_ssh_user:
    foo_vpc_network = yandex.VpcNetwork("fooVpcNetwork")
    foo_vpc_subnet = yandex.VpcSubnet("fooVpcSubnet",
        network_id=foo_vpc_network.id,
        v4_cidr_blocks=["10.2.0.0/16"],
        zone=zone)

    addr = yandex.VpcAddress("addr", external_ipv4_address=yandex.VpcAddressExternalIpv4AddressArgs(
        zone_id=zone,
    ))

    config = pulumi.Config()
    ssh_public_key = config.get('ssh_public_key')

    cores_memory_fraction = (
        (2, 0.5, 5),
        (2, 1, 50),
        (2, 2, 100),
    )

    default = yandex.ComputeInstance("default",
        boot_disk=yandex.ComputeInstanceBootDiskArgs(
            initialize_params=yandex.ComputeInstanceBootDiskInitializeParamsArgs(
                image_id="fd8kdq6d0p8sij7h5qe3",
            ),
        ),
        metadata={
            "ssh-keys": f'ubuntu:{ssh_public_key}',
        },
        network_interfaces=[yandex.ComputeInstanceNetworkInterfaceArgs(
            subnet_id=foo_vpc_subnet.id,
            nat_ip_address=addr.external_ipv4_address.address,
        )],
        platform_id="standard-v2",
        resources=yandex.ComputeInstanceResourcesArgs(
            cores=2,
            memory=2,
            core_fraction=100,
        ),
        zone=zone,
        scheduling_policy=yandex.ComputeInstanceSchedulingPolicyArgs(preemptible=True),
    )

    binding = InstanceAddressBinding(
        'binding',
        InstanceAddressBindingInputs(
            instance_id=default.id,
            ipv4_address=addr.external_ipv4_address.address,
        )
    )

    proxy_ssh_user = 'ubuntu'
    proxy_host = addr.external_ipv4_address.address

pulumi.export('ssh_connstring', Output.concat(proxy_ssh_user, '@', proxy_host))
# pulumi.export('db_uri', db_uri)
