import logging
import uuid
from collections import namedtuple
from typing import Optional

import pulumi
from pulumi import ResourceOptions, log
from pulumi.dynamic import CreateResult, Resource, ResourceProvider

from yc_cli import call_yc

log = logging.getLogger(__name__)
logging.basicConfig(filename='/tmp/pulumi.log', level=logging.DEBUG)


@pulumi.input_type
class InstanceAddressBindingInputs:
    def __init__(
        self, instance_id, ipv4_address
    ):
        self.instance_id = instance_id
        self.ipv4_address = ipv4_address


class InstanceAddressBindinglProvider(ResourceProvider):
    def create(self, inputs: InstanceAddressBindingInputs) -> CreateResult:
        log.debug('started')
        log.debug(str(inputs))
        if type(inputs) == dict:
            inputs = {k: inputs[k] for k in inputs.keys() if not k.startswith('__')}
            inputs = namedtuple('GenericDict', inputs.keys())(**inputs)
        log.debug(str(inputs))

        call_yc(
            'compute', 'instance', 'add-one-to-one-nat',
            inputs.instance_id,
            nat_address=inputs.ipv4_address,
            network_interface_index=0,
        )

        return CreateResult(id_=str(uuid.uuid4()), outs={})


class InstanceAddressBinding(Resource):
    def __init__(self, name, props: InstanceAddressBindingInputs,
                 opts: Optional[ResourceOptions] = None):
        log.debug(f'ctor {str(vars(props))}')
        super().__init__(InstanceAddressBindinglProvider(), name, {**vars(props)}, opts)
