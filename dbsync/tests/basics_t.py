import os

import backoff
import pytest
import sh
from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from pulumi import automation as auto

pytestmark = pytest.mark.yc

@backoff.on_exception(backoff.constant, sh.ErrorReturnCode_255, interval=1, max_time=60 * 5)
def ssh(ssh_connstring, id_rsa_path, remote_cmd):
    return sh.ssh('-oStrictHostKeyChecking=no', '-i', id_rsa_path, ssh_connstring, remote_cmd)


@pytest.fixture(scope='module')
def ssh_key():
    return rsa.generate_private_key(
        backend=crypto_default_backend(),
        public_exponent=65537,
        key_size=2048
    )


@pytest.fixture()
def id_rsa_path(ssh_key, tmp_path):
    id_rsa = ssh_key.private_bytes(
        crypto_serialization.Encoding.PEM,
        crypto_serialization.PrivateFormat.PKCS8,
        crypto_serialization.NoEncryption()
    )

    filepath = tmp_path / 'id_rsa'
    with open(filepath, 'wb') as f:
        f.write(id_rsa)
    os.chmod(filepath, 0o400)

    return filepath


@pytest.fixture(scope='module')
def id_rsa_pub(ssh_key):
    return ssh_key.public_key().public_bytes(
        crypto_serialization.Encoding.OpenSSH,
        crypto_serialization.PublicFormat.OpenSSH
    )


@pytest.fixture(scope='module')
def stack(id_rsa_pub):
    work_dir = os.path.join(os.path.abspath(os.path.dirname(__name__)), 'dbsync')
    try:
        stack = auto.select_stack(stack_name="ci.dbsync", work_dir=work_dir)
        stack.cancel()
    except auto.errors.StackNotFoundError:
        stack = auto.create_stack(stack_name="ci.dbsync", work_dir=work_dir)
    stack.destroy(on_output=print)
    stack.set_config('ssh_public_key', auto.ConfigValue(id_rsa_pub))
    stack.up(on_output=print)

    yield stack

    stack.destroy()


@pytest.fixture(scope='module')
def dev_stack():
    work_dir = os.path.join(os.path.abspath(os.path.dirname(__name__)), 'dbsync')
    try:
        stack = auto.select_stack(stack_name="dev.dbsync", work_dir=work_dir)
        stack.cancel()
    except auto.errors.StackNotFoundError:
        stack = auto.create_stack(stack_name="dev.dbsync", work_dir=work_dir)
    stack.up(on_output=print)

    yield stack

    stack.destroy()


@pytest.fixture(scope='module')
def db_uri(dev_stack):
    return dev_stack.outputs()['db_uri']


def may_connect_by_ssh_t(stack, id_rsa_path):
    res = ssh(stack.outputs()['ssh_connstring'].value, id_rsa_path, 'pwd')

    assert res.strip() == '/home/ubuntu'


@pytest.mark.skip(reason='wip')
def may_connect_by_ssh_to_dev_t(dev_stack, id_rsa_path):
    res = ssh(dev_stack.outputs()['ssh_connstring'].value, id_rsa_path, 'pwd')

    assert res.strip() == '/home/sutyrin'


@pytest.mark.skip(reason='wip')
def may_connect_to_postgres_inside_t(dev_stack, id_rsa_path, db_uri):
    res = ssh(dev_stack.outputs()['ssh_connstring'].value, id_rsa_path,
              f'psql {db_uri} "select now()"')

    print(res)

    assert False, 'wip'
