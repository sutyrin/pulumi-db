import os
from pprint import pprint

from sh import jq, yc  # noqa

resources = (  # in order of deletion, by dependence
    ('compute', 'instance'),
    ('postgres', 'cluster'),
    ('vpc', 'addr'),
    ('vpc', 'subnet'),
    ('vpc', 'network'),
)

folder_id = os.environ['YC_FOLDER_ID']
token = os.environ['YC_TOKEN']

def get_list(service, obj):
    return jq(
        yc(service, obj, 'list', token=token, format='json', folder_id=folder_id),
        '-r', '.[].id',
    ).splitlines()

def delete(service, obj, id_list):
    for i in id_list:
        yc(service, obj, 'delete', i, token=token)

def clear_folder():
    ids = {}
    for service, obj in resources:
        ids[(service, obj)] = get_list(service, obj)
    pprint(ids)
    for service, obj in resources:
        if ids[(service, obj)]:
            delete(service, obj, ids[(service, obj)])

if __name__ == '__main__':
    clear_folder()
